import React from 'react'
import { Text, Image, View, TouchableOpacity, Modal } from 'react-native'
import { Images } from './DevTheme'
import TabNavigator from 'react-native-tab-navigator'
import GestureView from 'react-native-gesture-view'
import QRCodeScanner from 'react-native-qrcode-scanner'
// Screens
import ComponentExamplesScreen from './ComponentExamplesScreen'
import ThemeScreen from './ThemeScreen'
import FaqScreen from './FaqScreen'
import PresentationScreen from './PresentationScreen'

// Styles
import styles from './Styles/TabOptionsStyle'

class TabOptions extends React.Component {
  constructor () {
    super()
    this.state = {
      selectedTab: 'home',
      showSesuatu: false,
      modalVisible: false
    }
  }
  render () {
    return (
      <GestureView onSwipeUp={() => { this.setState({ showSesuatu: true }) }}
        onSwipeDown={() => { this.setState({ showSesuatu: false }) }}
        swipeThreshold={30}
        style={styles.mainContainer}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert('Modal has been closed.')
          }}>
          <View style={{marginTop: 22}}>
            <View>
              <Text>Scanner QrCode</Text>
              <QRCodeScanner onRoad={(e) => alert(e.data)} />
            </View>
          </View>
        </Modal>
        <TabNavigator
          tabBarStyle={{ backgroundColor: '#210b2e' }}>
          <TabNavigator.Item
            selected={this.state.selectedTab === 'Components'}
            renderIcon={() => <Image source={Images.components} style={styles.SizeIcon} />}
            renderSelectedIcon={() => <Image source={Images.components} style={styles.SelectIcon} />}
            onPress={() => { this.setState({ selectedTab: 'Components' }) }}>
            <ComponentExamplesScreen navigation={this.props.navigation} />
          </TabNavigator.Item>
          <TabNavigator.Item
            selected={this.state.selectedTab === 'Plugin'}
            renderIcon={() => <Image source={Images.usageExamples} style={styles.SizeIcon} />}
            renderSelectedIcon={() => <Image source={Images.usageExamples} style={styles.SelectIcon} />}
            onPress={() => { this.setState({ selectedTab: 'Plugin' }) }}>
            <FaqScreen navigation={this.props.navigation} />
          </TabNavigator.Item>
          <TabNavigator.Item
            selected={this.state.selectedTab === 'ThemeScreen'}
            renderIcon={() => <Image source={Images.theme} style={styles.SizeIcon} />}
            renderSelectedIcon={() => <Image source={Images.theme} style={styles.SelectIcon} />}
            onPress={() => { this.setState({ selectedTab: 'ThemeScreen' }) }}>
            <ThemeScreen navigation={this.props.navigation} />
          </TabNavigator.Item>
          <TabNavigator.Item
            selected={this.state.selectedTab === 'Menu'}
            renderIcon={() => <Image source={Images.hamburger} style={styles.SizeIcon} />}
            renderSelectedIcon={() => <Image source={Images.hamburger} style={styles.SelectIcon} />}
            onPress={() => { this.setState({ selectedTab: 'Menu' }) }}>
            <PresentationScreen navigation={this.props.navigation} />
          </TabNavigator.Item>
        </TabNavigator>
        {this.state.showSesuatu
          ? <View style={styles.containerBot}>
            <TouchableOpacity onPress={() => this.setState({modalVisible: true})} >
              <Image source={Images.deviceInfo} style={styles.IconQr} />
            </TouchableOpacity>
            <Text style={styles.Text1}> &lt;&lt; Place your task button here </Text>
          </View>
          : null
        }

      </GestureView>
    )
  }
}

export default TabOptions
