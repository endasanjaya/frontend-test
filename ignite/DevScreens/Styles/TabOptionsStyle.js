import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../DevTheme/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    marginBottom: 36,
    paddingTop: Metrics.section
  },
  containerBot: {
    flexDirection: 'row',
    paddingVertical: 5,
    backgroundColor: '#3e243f',
    height: 70,
    borderTopWidth: 1,
    borderColor: 'white',
    paddingLeft: 35
  },
  SizeIcon: {
    height: 30,
    width: 30
  },
  SelectIcon: {
    height: 25,
    width: 25
  },
  IconQr: {
    height: 25,
    width: 25,
    marginTop: 25,
    paddingLeft: 5

  },
  Text1: {
    color: 'red',
    marginTop: 24,
    paddingLeft: 8
  }
})
