import React, {Component} from 'react'
import {TextInput, View, Image, TouchableOpacity, Text, AsyncStorage, alert} from 'react-native'
import images from '../../App/Themes/Images'

// request Http API sample Apiary
import axios from 'axios'
import styles from '../DevScreens/Styles/LoginsStyles'

export default class LoginScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: ''
    }
  }
  Login () {
    axios.post('https://private-8deb5d-yors1.apiary-mock.com/Auth', {
      username: this.state.username,
      password: this.state.password
    })
.then((respone) => {
  console.log('Test Post', respone)
  AsyncStorage.setItem('Token', JSON.stringify({token: respone.data.access_token}))
  alert('Login Sucsess !!')
  this.props.navigation.navigate('TabOptions')
})
.catch((error) => {
  return (error)
})
  }
  render () {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer} >
          <Image source={images.launch} style={styles.logo} />
        </View>
        <TextInput
          placeholder='Username'
          placeholderTextColor='rgba(255,255,255,0.7)'
          returnKeyType='next'
          onSubmitEditing={() => this.passwordInput.focus()}
          style={styles.Input}
          onChangeText={(username) => this.setState({username})}
          underlineColorAndroid='transparent'
/>
        <TextInput
          placeholder='Password'
          placeholderTextColor='rgba(255,255,255,0.7)'
          returnKeyType='go'
          secureTextEntry
          underlineColorAndroid={'transparent'}
          style={styles.Input}
          onChangeText={(password) => this.setState({password})}
          ref={input => (this.passwordInput = input)} />
        <TouchableOpacity onPress={() => this.Login()} style={styles.buttomContainer}>
          <Text style={styles.buttomText}> Sign In</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
